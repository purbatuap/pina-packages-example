import formula from '../dist/index';

const pmt = () => {
  console.log('pmt:', formula.pmt(2.5 / 100 / 12, 24, 6000, 0, 0));
}

const generalGoal = () => {
  const result = formula.goal.general.suggestMonthly({
    target: 600000000,
    duration_in_months: 60,
    annual_interest_rate: 3.88,
  });

  console.log('goal:general:monthly-recommendation', result);
}


const setGoalConstant = () => {
  console.log('constant:before');
  formula.goal.constant.print();

  formula.goal.constant.set('OFF_TRACK_RATIO_THRESHOLD', -12.5);

  console.log('constant:after');
  formula.goal.constant.print();
}

(function examples() {
  console.log(`version: ${formula.version}`);

  pmt();
  generalGoal();
  setGoalConstant();
})();

