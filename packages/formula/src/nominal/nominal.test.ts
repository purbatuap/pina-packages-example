import nominal from './nominal';

describe('NOMINAL', () => {
  test('Calculates nominal interest rate 1', () => {
    expect(nominal(10/100, 4)).toBeCloseTo(0.09_645_475_634);
  });

  test('Calculates nominal interest rate 2', () => {
    expect(nominal(3/100, 2)).toBeCloseTo(0.02_977_831_302);
  });
});