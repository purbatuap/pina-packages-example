/**
 * Calculates the annual nominal interest rate
 * given the effective rate and number of
 * compounding periods per year.
 * 
 * @param {number} rate - the effective interest rate per year
 * @param {number} periods - the number of compounding periods per year
 * @see https://support.google.com/docs/answer/3093234?hl=en
 */
const calculation = (rate: number, periods: number) => {
  if (periods < 1) {
    throw new Error("Argument `periods` should not less than 1");
  }

  return (Math.pow(rate + 1, 1 / periods) - 1) * periods;
};

export default calculation;
