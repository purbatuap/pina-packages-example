Calculates the annual nominal interest rate given the effective rate and number of compounding periods per year.

### Usage
```js
const result = formula.nominal(3.88/100, 12);
```

### Arguments
| Arg     | Type     | Default     | Description                            |
|---------|----------|-------------|----------------------------------------|
| rate    | `number` | `undefined` | Effective interest rate per year       |
| periods | `number` | `undefined` | Number of compounding periods per year |

### Test & Development
```
$ npm run test:watch --workspace=@anip/formula -- nominal.test.ts
```