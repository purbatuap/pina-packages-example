import ci from './ci';

describe('CI (Compound Interest)', () => {
  test('Calculates compound interest with zero monthly investment', () => {
    const periods = 60;
    const monthlyInvestment = 0;
    const result = ci({
      monthly_investment: monthlyInvestment,
      annual_interest_rate: 3.88,
      number_of_periods_in_months: periods,
    });

    const lastInvestment = result.investments[result.investments.length - 1];

    expect(lastInvestment.month).toEqual(periods);
    expect(result.investments.length).toEqual(periods + 1);
    expect(lastInvestment.interest).toEqual(0);
    expect(result.total_contribution).toEqual(0);
    expect(result.total_interest).toEqual(0);
    expect(result.total_investment).toEqual(0);
    expect(result.total_investment_1y).toEqual(0);
  });

  test('Calculates compound interest with zero number of periods', () => {
    const periods = 0;
    const monthlyInvestment = 10_000_000;
    const result = ci({
      monthly_investment: monthlyInvestment,
      annual_interest_rate: 3.88,
      number_of_periods_in_months: periods,
    });

    const lastInvestment = result.investments[result.investments.length - 1];

    expect(lastInvestment).toBeUndefined();
    expect(result.investments.length).toEqual(periods);
    expect(result.total_contribution).toEqual(0);
    expect(result.total_interest).toEqual(0);
    expect(result.total_investment).toEqual(0);
    expect(result.total_investment_1y).toEqual(0);
  });

  test('Calculates simple compound interest', () => {
    const periods = 60;
    const monthlyInvestment = 10_000_000;
    const annualRate = 3.88;

    const result = ci({
      monthly_investment: monthlyInvestment,
      annual_interest_rate: 3.88,
      number_of_periods_in_months: periods,
    });

    const lastInvestment = result.investments[result.investments.length - 1];

    expect(lastInvestment.month).toEqual(periods);
    expect(result.investments.length).toEqual(periods + 1);
    expect(lastInvestment.interest).toEqual(2_096_499.3_040_233_317);
    expect(result.total_contribution).toEqual(600_000_000);
    expect(result.total_interest).toEqual(61_949_842.45_169_368);
    expect(result.total_investment).toEqual(661_949_842.4_516_937);
    expect(result.total_investment_1y).toEqual(122_119_333.23_801_756);
  });

  test('Calculates simple compound interest 2', () => {
    const periods = 60;
    const monthlyInvestment = 7373137;
    const annualRate = 5;

    const result = ci({
      monthly_investment: monthlyInvestment,
      annual_interest_rate: annualRate,
      number_of_periods_in_months: periods,
    });

    const lastInvestment = result.investments[result.investments.length - 1];

    expect(lastInvestment.month).toEqual(periods);
    expect(result.investments.length).toEqual(periods + 1);
    expect(lastInvestment.interest).toEqual(2_037_061.8_108_865_921);
    expect(result.total_contribution).toEqual(442_388_220);
    expect(result.total_interest).toEqual(59_648_821.944_630_854);
    expect(result.total_investment).toEqual(502_037_041.944_631);
    expect(result.total_investment_1y).toEqual(90_487_395.46_884_234);
  });
});