import mcir from '../mcir';

export type CompoundInterestArgs = {
  monthly_investment: number,
  annual_interest_rate: number,
  number_of_periods_in_months: number,
}

export type MonthlyInvestmentItem = {
  month: number,
  contribution: number,
  interest: number,
  total_contribution: number,
  total_interest: number,
  total_investment: number,
}

export type CompoundInterestResult = {
  total_contribution: number,
  total_interest: number,
  total_investment: number,
  total_investment_1y: number,
  investments: Array<MonthlyInvestmentItem>,
}


/**
 * Calculates compound interest
 *
 * @param args - compound interest arguments
 * @returns {object}
 * 
 * @see https://www.investor.gov/financial-tools-calculators/calculators/compound-interest-calculator
 */
const calculation = (args: CompoundInterestArgs): CompoundInterestResult => {
  const result: CompoundInterestResult = {
    total_contribution: 0,
    total_interest: 0,
    total_investment: 0,
    total_investment_1y: 0,
    investments: [],
  };

  if (args.number_of_periods_in_months <= 0) {
    return result;
  }

  // initial
  result.investments.push({
    month: 0,
    contribution: args.monthly_investment,
    interest: 0,
    total_contribution: args.monthly_investment,
    total_interest: 0,
    total_investment: args.number_of_periods_in_months > 0  ? args.monthly_investment : 0,
  });

  const monthlyRate = mcir(args.annual_interest_rate);

  for(let i = 1; i <= args.number_of_periods_in_months; i++) {
    const prevTotalInvestment = result.investments[i-1].total_investment;
    const interest = prevTotalInvestment * monthlyRate;
    const totalInvestment = prevTotalInvestment + args.monthly_investment + interest;

    result.investments.push({
      month: i,
      contribution: args.monthly_investment,
      interest: interest,
      total_contribution: result.investments[i-1].total_contribution + args.monthly_investment,
      total_interest: result.investments[i-1].total_interest + interest,
      total_investment: totalInvestment,
    });

    if (i === args.number_of_periods_in_months) {
      result.investments[i].total_contribution = result.investments[i].total_contribution - args.monthly_investment;
      result.investments[i].total_investment = result.investments[i].total_investment - args.monthly_investment;
    }
  }

  const n = result.investments.length;

  result.total_contribution = result.investments[n - 1]?.total_contribution || 0;
  result.total_interest = result.investments[n - 1]?.total_interest || 0;
  result.total_investment = result.investments[n - 1]?.total_investment || 0;
  result.total_investment_1y = result.investments[11]?.total_investment || 0;

  return result; 
}

export default calculation;