import pmt from '.';

describe('PMT (Payment)', () => {
  /**
   * @see https://docs.google.com/spreadsheets/u/0/d/19lPkDuCrbcelpBy2d6eD20ZQ_x5YwH3p0JezhT6wvF4/pub?output=html
   */
  it('should returns correct calculations based on google spreadsheet example', () => {
    const rate = 2.5/100; // 2.5%
    const nper = 24;
    const pv = 6000;
    const fv = 0;
    const type = 0;

    const result = pmt(rate/12, nper, pv, fv, type);

    expect(result).toBeCloseTo(-256.5_623_533);
  });

  it('should returns -250 with given arguments and if rate=0', () => {
    const rate = 0;
    const nper = 24;
    const pv = 6000;
    const fv = 0;
    const type = 0;

    const result = pmt(rate/12, nper, pv, fv, type);

    expect(result).toBeCloseTo(-250);
  });

  it('should returns -50.31 with given arguments and if end_or_beginning=1', () => {
    const rate = 5/100; // 5%
    const nper = 60;
    const pv = 1000;
    const fv = 0;
    const type = 1;

    const result = pmt(rate, nper, pv, fv, type);

    expect(result).toBeCloseTo(-50.31);
  });

  it('should returns NaN when all arguments are zero', () => {
    const rate = 0;
    const nper = 0;
    const pv = 0;
    const fv = 0;
    const type = 0;

    const result = pmt(rate, nper, pv, fv, type);

    expect(result).toEqual(NaN);
  });
});