The PMT function calculates the periodic payment for an annuity investment based on constant-amount periodic payments and a constant interest rate.

### Usage
```js
const result = formula.pmt(3.88/100, 12, 6000);
```

### Arguments
| Arg                           | Type     | Default     | Description                                                  |
|-------------------------------|----------|-------------|--------------------------------------------------------------|
| rate                          | `number` | `undefined` | Interest rate                                                |
| number_of_periods             | `number` | `undefined` | Number of payments to be made                                |
| present_value                 | `number` | `undefined` | Current value of the annuity                                 |
| future_value (optional)       | `number` | `0`         | Future value remaining after the final payment has been made |
| end_or_beginning (optional)   | `number` | `0`         | Whether payments are due at the end (0) or beginning (1) of each period |


### Test & Development
```
$ npm run test:watch --workspace=@anip/formula -- pmt/pmt.test.ts
```