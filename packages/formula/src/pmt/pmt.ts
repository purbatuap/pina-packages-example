/**
 * Calculates the periodic payment for an annuity investment
 * 
 * @param rate 
 * @param number_of_periods 
 * @param present_value 
 * @param future_value
 * @param end_or_beginning
 * 
 * @see https://support.google.com/docs/answer/3093185?hl=en
 * @see https://gist.github.com/Nitin-Daddikar/43899765e30274ec739f44ebbac434c3#file-excel-js-L87
 */
const calculation = (
  rate: number,
  number_of_periods: number,
  present_value: number,
  future_value?: number,
  end_or_beginning?: number
) => {
  const periods = number_of_periods || 0;
  const present = present_value || 0;
  const future = future_value || 0;
  const type = end_or_beginning ? 1 : 0;

  if (rate === 0) {
    return -1 * (present + future) / periods;
  }

  const term = Math.pow(1 + rate, periods);
  const result = future * rate / (term - 1) + present * rate / (1 - 1 / term); // mobile-app-calculation

  if (type === 1) {
    return -1 * result / (1 + rate);
  }

  return -1 * result;
}

export default calculation;
