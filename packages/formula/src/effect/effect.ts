/**
 * Calculates the annual effective interest rate
 * given the nominal rate and number of
 * compounding periods per year.
 * 
 * @param {number} rate - the nominal interest rate per year
 * @param {number} periods - the number of compounding periods per year
 * @see https://support.google.com/docs/answer/3093223?hl=en&ref_topic=3105398
 */
const calculation = (rate: number, periods: number) => {
  if (rate <= 0) {
    throw new Error("Argument `rate` should greater than 0");
  }

  if (periods < 1) {
    throw new Error("Argument `periods` should not less than 1");
  }

  // Truncate periods if it is not an integer
  periods = Math.floor(periods);

  // Return effective annual interest rate
  return Math.pow(1 + rate / periods, periods) - 1;
};

export default calculation;