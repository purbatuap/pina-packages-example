import effect from './effect';

describe('EFFECT', () => {
  test('Calculates effective interest rate 1', () => {
    expect(effect(8/100, 2)).toBeCloseTo(0.0_816);
  });

  test('Calculates effective interest rate 2', () => {
    expect(effect(10/100, 4)).toBeCloseTo(0.1_038_128_906);
  });

  test('Calculates effective interest rate 3', () => {
    expect(effect(3.88/100, 12)).toBeCloseTo(0.03_949_748_431);
  });
});