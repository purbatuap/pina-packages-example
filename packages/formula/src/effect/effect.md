Calculates the annual effective interest rate given the nominal rate and number of compounding periods per year.

### Usage
```js
const result = formula.effect(6.68/100, 12);
```

### Arguments
| Arg     | Type     | Default     | Description                            |
|---------|----------|-------------|----------------------------------------|
| rate    | `number` | `undefined` | Nominal interest rate per year         |
| periods | `number` | `undefined` | Number of compounding periods per year |


### Test & Development
```
$ npm run test:watch --workspace=@anip/formula -- effect.test.ts
```