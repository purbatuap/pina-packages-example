Goal related formulas.

### Constant

Set constant
```js
formula.goal.constant.set('OFF_TRACK_RATIO_THRESHOLD', -8.5);
```

Get constant
```js
const offTrackRatio = formula.goal.constant.get('OFF_TRACK_RATIO_THRESHOLD');
```

