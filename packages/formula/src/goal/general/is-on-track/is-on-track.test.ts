import isOnTrack from '.';

describe('On Track Status', () => {
  it('should returns true', () => {
    const result = isOnTrack({
      annual_interest_rate: 15.46,
      invested_value: 200_000.005,
      target: 300_000_000,
      duration_in_months: 60,
      investments: [
        {
          date: "2023-01-01",
          value: 200_000.00,
          invested: 200_000.00
        }
      ]
    });

    expect(result).toBe(false);
  });
});