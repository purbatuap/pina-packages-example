import isOnTrack from './is-on-track'
import type { InvestmentItem, IsOnTrackArgs } from './is-on-track'

export { isOnTrack as default };
export type { InvestmentItem, IsOnTrackArgs };
