import constant from '../../constant'
import type { GeneralGoalArgs } from '../suggest-monthly'
import suggestMonthly from '../suggest-monthly'
import pmt from '../../../pmt'
import calculatePMT from '../../../temp/calculate-monthly-pmt/calculate-monthly-pmt'

export type InvestmentItem = {
  date: string,
  invested: number,
  value: number,
}

export type IsOnTrackArgs = GeneralGoalArgs & ({
  invested_value: number,
  investments: Array<InvestmentItem>
});

// TODO: need fix
const calculation = (args: IsOnTrackArgs): boolean => {
  const investmentsLength = args.investments?.length || 0;
  const duration = args?.duration_in_months - investmentsLength;
  // const monthlyInvestment = pmt(args?.annual_interest_rate/100, duration, -1 * args?.invested_value, args?.target);
  // const monthlyInvestmentActual = pmt(args?.annual_interest_rate/100, args?.duration_in_months, -1 * args?.invested_value, args?.target);

  // const projection = suggestMonthly({
  //   duration_in_months: args?.duration_in_months,
  //   annual_interest_rate: args?.annual_interest_rate,
  //   target: args?.target,
  // });

  const compound = calculatePMT(args?.invested_value, args.investments, args?.annual_interest_rate/100, duration);

  const lastElement = compound[compound.length - 1];
  const ratio = (((lastElement.initial + lastElement.saving) - args.target) / args.target) * 100;

  return ratio > constant.get("OFF_TRACK_RATIO_THRESHOLD");
}

export default calculation;
