Check goal on track status.

### Usage
```js
const result = formula.goal.general.isOnTrack({
  target: 600_000_000,
  duration_in_months: 60,
  annual_interest_rate: 3.88,
  investments: [
    {
      date: "2023-01-01",
      invested: 0,
      value: 0,
    },
    {
      date: "2023-02-01",
      invested: 0,
      value: 0,
    }
  ],
})
```

### Arguments
| Arg                           | Type                    | Default     | Description                                                  |
|-------------------------------|-------------------------|-------------|-------------------------------------|
| target                        | `number`                | `undefined` | Goal target amount                  |                                   |
| duration_in_months            | `number`                | `undefined` | Number of months                    |
| annual_interest_rate          | `number`                | `undefined` | Interest rate                       |
| investments                   | `Array<InvestmentItem>` | `undefined` | Array of user monthly investments   |


### Test & Development
```
$ npm run test:watch --workspace=@anip/formula -- /general/is-on-track/is-on-track.test.ts
```