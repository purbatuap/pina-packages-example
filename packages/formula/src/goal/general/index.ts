import suggestMonthly, { GeneralGoalArgs, GeneralGoalResult } from './suggest-monthly';
import isOnTrack from './is-on-track';
import type { IsOnTrackArgs, InvestmentItem } from './is-on-track'

export { suggestMonthly, isOnTrack };
export type {  GeneralGoalArgs, GeneralGoalResult, IsOnTrackArgs, InvestmentItem };