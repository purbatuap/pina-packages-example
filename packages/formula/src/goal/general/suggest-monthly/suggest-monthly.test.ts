import calculation from './suggest-monthly';

describe('General Goal: Suggest Monthly Investment', () => {
  test('Calculates suggested monthly investment, annual rate: 6.39%', () => {
    const interestRate = 6.39;
    const duration = 48;
    const target = 250_000_000;

    const result = calculation({
      target: target,
      duration_in_months: duration,
      annual_interest_rate: interestRate,
    });

    expect(result.formatted_suggested_monthly_investment).toEqual('4.601.593');
    expect(result.formatted_total_contribution).toEqual('220.876.473');
    expect(result.formatted_total_interest).toEqual('30.417.309');
    expect(result.formatted_total_investment).toEqual('251.293.782');

    expect(result.formatted_duration_in_years).toBe('4');
  });
});
