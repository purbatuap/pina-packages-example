import formatCurrency from '../../../utils/format/currency';
import ci, { CompoundInterestResult } from '../../../ci';
import mcir from '../../../mcir';
import pmt from '../../../pmt';


export type GeneralGoalArgs = {
  target: number,
  duration_in_months: number,
  annual_interest_rate: number,
};

export type GeneralGoalResult = {
  suggested_monthly_investment: number,
  total_contribution: number,
  total_interest: number,
  total_investment: number,
  formatted_suggested_monthly_investment: string,
  formatted_total_contribution: string,
  formatted_total_interest: string,
  formatted_total_investment: string,
  formatted_duration_in_years: string,
};

/**
 * Suggest monthly investment
 */
const calculation = (args: GeneralGoalArgs): GeneralGoalResult => {
  const result = {
    suggested_monthly_investment: 0,
    total_contribution: 0,
    total_interest: 0,
    total_investment: 0,
    formatted_suggested_monthly_investment: '',
    formatted_total_contribution: '',
    formatted_total_interest: '',
    formatted_total_investment: '',
    formatted_duration_in_years: '',
  };

  const monthlyRate = mcir(args.annual_interest_rate);
  const monthlyInvestment = pmt(monthlyRate, args.duration_in_months, 0, args.target);

  const compound: CompoundInterestResult = ci({
    annual_interest_rate: args?.annual_interest_rate,
    number_of_periods_in_months: args?.duration_in_months,
    monthly_investment: monthlyInvestment,
  });

  result.suggested_monthly_investment = monthlyInvestment;
  result.total_contribution = compound.total_contribution;
  result.total_interest = compound.total_interest;
  result.total_investment = compound.total_investment;
  result.formatted_suggested_monthly_investment = formatCurrency(result.suggested_monthly_investment);
  result.formatted_total_contribution = formatCurrency(result.total_contribution);
  result.formatted_total_interest = formatCurrency(result.total_interest);
  result.formatted_total_investment = formatCurrency(result.total_investment);
  result.formatted_duration_in_years = (args.duration_in_months / 12).toString();

  return result;
}

export default calculation;
