Calculates suggested monthly investment.

### Usage
```js
const result = formula.goal.general.suggestMonthly({
  target: 600_000_000,
  duration_in_months: 60,
  annual_interest_rate: 3.88,
});
```

### Arguments
| Arg                           | Type     | Default     | Description                                                  |
|-------------------------------|----------|-------------|------------------------------|
| target                        | `number` | `undefined` | Goal target amount           |                                   |
| duration_in_months            | `number` | `undefined` | Number of months             |
| annual_interest_rate          | `number` | `undefined` | Interest rate                |


### Test & Development
```
$ npm run test:watch --workspace=@anip/formula -- /general/suggest-monthly/suggest-monthly.test.ts
```