import suggestMonthly from './suggest-monthly'

export { suggestMonthly as default }
export type { GeneralGoalArgs, GeneralGoalResult } from './suggest-monthly'

