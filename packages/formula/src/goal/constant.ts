const items: Record<string, number | string> = {
  OFF_TRACK_RATIO_THRESHOLD: -2.5,
}

const set = (key: string, value: number | string) => {
  if (!items[key]) {
    return null;
  }

  items[key] = value;

  return true;
}

const get = (key: string): number | string => {
  return items[key];
}

const print = () => {
  for(const key of Object.keys(items)) {
    console.log(`${key}: ${items[key]}`);
  }
}

const constant = { get, set, print };

export default constant;
export { get, set, print };



