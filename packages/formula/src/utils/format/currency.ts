interface ICustomOptions {
  removeCurrencyDisplay: boolean,
};

export type FormatCurrencyOptions = Intl.NumberFormatOptions & ICustomOptions;

/**
 * Format currency, only IDR
 * @param amount 
 * @param options 
 * @returns {string}
 */
const formatter = (amount: number, options?: FormatCurrencyOptions) => {
  const opts = {
    locale: "id-ID",
    currencyDisplay: "code",
    currencySign: "standard",
    signDisplay: "never",
    roundingMode: "trunc",
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
    removeCurrencyDisplay: true,
    ...options,
    style: "currency",
    currency: "IDR",
  };

  let result = new Intl.NumberFormat(opts.locale, opts as Intl.NumberFormatOptions).format(amount);

  if (opts?.removeCurrencyDisplay) {
    if (
      opts?.currencyDisplay === "symbol" ||
      opts?.currencyDisplay === "narrowSymbol"
    ) {
      result = result.replace("Rp", "").trim();
    } else if (opts?.currencyDisplay === "code") {
      result = result.replace(opts.currency, "").trim();
    } else if (opts?.currencyDisplay === "name") {
      result = result.replace("Rupiah Indonesia", "").trim();
    }
  }

  return result;
};

export default formatter;
