/**
 * Calculates monthly compound interest rate from annual interest rate
 *
 * @param {number} annual_interest_rate
 * @returns {number}
 */
const calculation = (annual_interest_rate: number) => {
  return Math.pow(1 + (annual_interest_rate / 100), (1 / 12)) - 1;
}

export default calculation;