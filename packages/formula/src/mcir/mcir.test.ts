import mcir from '.';

describe('MCIR (Monthly Compound Interest Rate)', () => {
  test('Calculates monthly compound interest rate', () => {
    const annualInterestRate = 3.88;

    expect(mcir(annualInterestRate)).toEqual(0.0_031_772_200_986_699_417);
  });
});