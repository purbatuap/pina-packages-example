
type ResultItem = {
  initial: number,
  saving: number
}
type Result = Array<ResultItem>

const calculatePMT = (
  initial: number,
  monthly: any[],
  annual_rate: number,
  total_month: number,
  addInitial = false,
): Result => {
  // console.log({ initial, monthly, annual_rate, total_month, addInitial })

  const monthly_rate = Math.pow(1 + annual_rate, 1 / 12) - 1;

  const arr: Array<ResultItem> = [];
  for (let i = 0; i < total_month; i++) {
    // console.log('arr', arr)
    let start_initial = initial;
    const monthly_value = Array.isArray(monthly) && monthly?.length > 0  ? monthly[i] : monthly;
    // console.log('monthly_value', monthly_value)

    let start_saving = monthly_value;

    let saving_mo = 0;

    let saving = 0;

    if (i > 0) {
      start_initial = arr[i - 1].initial;
      if (i > 1) start_saving = arr[i - 1].saving;
    }

    saving = start_saving * monthly_rate;
    saving_mo = start_saving
      + saving
      + (i > 0 ? monthly_value + monthly_value * monthly_rate : 0);

    if (addInitial && i === 0) saving_mo += initial;

    const interest = start_initial * monthly_rate;

    arr.push({
      initial: start_initial + interest,
      saving: saving_mo == 0 && i > 0 ? arr[i - 1].saving : saving_mo,
    });
  }

  return arr;
}

export default calculatePMT;
