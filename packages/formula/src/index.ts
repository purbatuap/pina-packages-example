import { version } from '../package.json';
import pmt from './pmt';
import effect from './effect';
import nominal from './nominal';
import * as goal from './goal';

const formula = {
  version,
  pmt,
  effect,
  nominal,
  goal,
};

export default formula; // es
export { version, pmt, effect, nominal, goal }; // cjs
