# @anip/formula

## 1.1.1

### Patch Changes

- - Update how to export the modules
  - Update README.md
  - Add examples

## 1.1.0

### Minor Changes

- Initial setups
- Add pmt calculations
- Add effective interest rate calculations
- Add nominal interest rate calculations
