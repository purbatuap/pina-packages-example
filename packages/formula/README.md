# Formula


## Installation

Using npm:
```
$ npm install --save @anip/formula
```

Using yarn:
```
$ yarn add @anip/formula
```

## Usage
Once the package is installed, you can import the library using `import` or `require` approach:

```
import formula from '@anip/formula`;

console.log(formula.version);

// or

const formula = require('@anip/formula);

console.log(formula.version);
```

## API
- [pmt](src/pmt/pmt.md)
- [effect](src/effect/effect.md)
- [nominal](src/nominal/nominal.md)
- [goal](src/goal/goal.md)
  - general
    - [suggestMonthly](src/goal/general/suggest-monthly/suggest-monthly.md)
    - [isOnTrack](src/goal/general/is-on-track/is-on-track.md)
  - pension
    - [suggestMonthly](src/goal/pension/suggest-monthly/suggest-monthly.md)
    - [isOnTrack](src/goal/pension/is-on-track/is-on-track.md)


## Test & Development
From root project:
```
$ npm run test:watch --workspace=@anip/formula
```

From formula directory:
```
$ npm run test:watch
```

## Run Examples
Run the package as dev & watch mode:
```
$ npm run dev --workspace=@anip/formula
```

Run the examples:
```
$ cd packages/formula/examples
$ npm start
```

## Versioning & Add Changelogs

### Create changelog draft:
```
$ npm run changeset
```

Update draft inside `.changeset/newly-added-draft-file-name.json`

### Bump version & update `CHANGELOG.md`
```
$ npm run changeset:version
```

## Publishing
Publish the package to npm:

Need authentication (one time) before publishing to npm, run:
```
$ npm adduser
```
### Publish:
```
$ npm publish --workspace=@anip/formula
```